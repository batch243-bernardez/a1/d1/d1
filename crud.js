let http = require("http");

const port = 4000

	// Mock Database
	let directory = [
		{
			"name" : "Brandon",
			"email" : "brandon@mail.com"
		},
		{
			"name" : "Jobert",
			"email" : "jobert@mail.com"
		}
	]

const server = http.createServer((request,response) => {

	if(request.url == "/users" && request.method == "GET"){
		response.writeHead(200,{'Content-Type': 'application/json'});
		response.write(JSON.stringify(directory));
		response.end();
	}

	if(request.url == "/users" && request.method == "POST"){
		let requestBody = "";
		request.on("data", function(data){
		requestBody += data;

		});

		request.on("end", function(){
			console.log(typeof requestBody);

			requestBody = JSON.parse(requestBody);

			let newUser = {
				"name" : requestBody.name,
				"email" : requestBody.email
			};

			directory.push(newUser);
			console.log(directory);

			response.writeHead(200,{'Content-Type': 'application/json'});
			response.write(JSON.stringify(newUser));
			response.end();
		});
	}

}).listen(4000);

console.log("Server running at localhost 4000");