// HTTP Routing Methods: Get, Post, Put, Delete
/*
	- HTTP Method of the incoming request can be accessed via method property of the request parameter
	- The method "GET" means that we will be retrieving or reading an information
	- The method "POST" means that we will request the '/items' path and sends information
*/

let http = require("http")

const port = 4000

http.createServer(function(request,response){

	if(request.url == "/items" && request.method == "GET"){
		response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end("Data retrieved from the database");
	}

	else if(request.url == "/items" && request.method == "POST"){
		response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end("Data to be sent to the database");
	}

	

}).listen(4000);

console.log('Server running at localhost: 4000');